#!/bin/env python3

import re
import csv
rSeparator = re.compile(r"\"((?:[^\"]*(?:\\\")?)*)\",(.*)\n?")

def __csvline2record(line):
	m = rSeparator.match(line)
	if not m:
		print("Bad line:\n" + line)
		return (None,None)
	return (m.group(1).replace(r'\"', '\"'), eval(m.group(2)))

def csv2dict(filename):
	d = {}
	with open(filename, 'r') as csvfile:
		for line in csvfile.readlines():
			k, v = __csvline2record(line)
			d[k] = v
	return d

def csv2arr(filename):
	d = []
	with open(filename, 'r') as csvfile:
		for line in csvfile.readlines():
			l = line.rstrip()
			if l not in d:
				d.append(l)
	return d

def dict2csv(filename, d):
	f = open(filename, "w")
	for key in sorted(d.keys()):
		value = d[key]
		if type(value) == str:
			s = "\"" + value.replace('\"', r'\"') + "\""
		else:
			s = str(value)
		k = key.replace('\"', r'\"')
		S = f"\"{k}\",{s}\n"
		f.write(S)
	f.close()

if __name__ == '__main__':
	a={'key1': 1234, 'key2,nnoe': 54321, '"key3"': (1, 2, 3), "'key4'": (1,), 'key6': 'Test "1"'}
	dict2csv("file1.csv", a)
	b = csv2dict("file1.csv")
	print(b)
